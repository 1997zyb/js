GIT
    用户名：ZHYWGP
    密码：zybwgp1314520
    网易邮箱账号：zyb3287308097@163.com
    密码：zybwgp1314520


    
创建一个版本库：mkdir testgit

显示当前目录：pwd

git init：把这个目录变为git可以管理的仓库


git add readme.txt：添加到暂存区里面

git commit -m 'readme.txt提交'：告诉Git，把文件提交到仓库。

git status：查看是否还有未提交的文件

git diff readme.txt：查看readme.txt文件到底改了什么

git log：查看历史纪录 （可以看到版本号）

git log --pretty=oneline：是上一步的简化



回退命令：
    git reset --hard HEAD^
    git reset --hard HEAD^^
    git reset --hard HEAD~100

回退后查看readme.txt内容：cat readme.txt

回退之后再想回来：git reflog，首先要查看版本号
利用版本号回复：git reset --hard 版本号






工作区与暂存区：

    工作区：就是电脑上看到的目录，
    版本库：工作区有一个.git目录，这个不属于工作区，这是版本库
            版本库中有stage（暂存区），还有Git为我们自动创建的第一个分支


Git提交文件两步：
    
    （1）git add 文件名
    （2）git commit -m '文件介绍'




git checkout -- 文件名：丢弃工作区中的修改

rm 文件名：删除目录下的文件

想要彻底删除，需要再commit一次
想要恢复：git checkout -- b.txt





创建并切换分支：git checkout -b name

创建分支：git branch 分支

查看当前分支：git branch

切换分支：git checkout 分支
合并分支：git merge 分支

合并完后可以删除分支：git branch -d 分支