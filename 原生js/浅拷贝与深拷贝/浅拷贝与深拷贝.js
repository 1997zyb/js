var s = 'string'
s[0] = 'm'
console.log(s);
// console.log(s.split('t',1));



var arr = ['a', 'b', 'c']
arr[0] = 'n'
console.log(arr);







var obj1 = {
    name: '张三',
    age: 12,
    lang: [1, [2, 3], [4, 5, 6]]
}
// console.log(obj1['name']);

var obj2 = obj1;
var obj3 = show(obj1);

function show(obj) {
    var dst = {};
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            dst[prop] = obj[prop]
        }
    }
    return dst;
}

obj2.name = '李四';
console.log(obj1.name);
obj3.age = 20;
console.log(obj1.age);

obj2.lang[1] = ['二', '三'];
obj3.lang[2] = ['四', '五', '六'];

console.log(obj1.lang);
console.log(obj2.lang);
console.log(obj3.lang);

console.log(obj1 == obj2);
console.log(obj1 == obj3);











// 对象赋值
var obj1 = {
    name: '张三',
    arr: [1, [2, 3], 4],
}
var obj2 = obj1;
obj2.name = '阿浪';
obj2.arr[1] = [6, 7];
console.log(obj1);
console.log(obj2);



// 浅拷贝
var obj1 = {
    name: '张三',
    arr: [1, [2, 3], 4],
}

var obj2 = show(obj1)
obj2.name = '李四';
obj2.arr[1] = [7, 8]
function show(obj1) {
    var res = {};
    for (var val in obj1) {
        if (obj1.hasOwnProperty(val)) {
            res[val] = obj1[val]
        }
    }
    return res
}
console.log(obj1);
console.log(obj2);








// 深拷贝
var obj1 = {
    name: '张三',
    arr: [1, [2, 3], 4],
}

var obj2 = deepClone(obj1)
obj2.name = '李思思';
obj2.arr[1] = [5, 6, 7]

function deepClone(obj) {
    if (obj === null) return obj;
    if (obj instanceof Date) return new Date(obj);
    if (obj instanceof RegExp) return new RegExp(obj);
    if (typeof obj !== 'object') return obj;

    var cloneObj = new obj.constructor();
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            cloneObj[key] = deepClone(obj[key]);
        }
    }
    return cloneObj
}
console.log(obj1);
console.log(obj2);







// 浅拷贝的实现方式
// Object.assign()可以把任意多个源对象自身可枚举属性拷贝到目标对象，然后返回目标对象
// 对象只有一层时，是深拷贝；对象是两层时是浅拷贝。
var stu1 = {
    name: '张三',
    age: 12,
    arr: [[1, 2], [3, 4], 5],
    prop: { sex: '男', height: 1.81, weight: 164, }
}
var obj2 = Object.assign({}, stu1)
console.log(obj2);
obj2.name = '李四';
// obj2.arr[0] = [7,8]; 
obj2.prop.sex = '女';
console.log(obj2);
console.log(stu1);







// 函数库lodash的_clone方法   浅拷贝
var _ = require('lodash');
var stu1 = {
    name: '张三',
    age: 12,
    arr: [[1, 2], [3, 4], 5],
    prop: { sex: '男', height: 1.81, weight: 164, }
}
var obj2 = _.console(obj1);
obj2.prop.sex = '女'
console.log(stu1);





// 展开运算符...   浅拷贝
var obj1 = {
    name: 'kobe',
    address: { x: 100, y: 200 }
}
var obj2 = { ...obj1 }
obj2.name = 'wabe';
obj2.address.x = 400;
console.log(obj1, obj2);




// concat()   浅拷贝
var arr = [1, 3, { name: 'kobe', age: 10 }]
var arr2 = arr.concat();
arr2[2].name = 'wabe'
console.log(arr, arr2);



// slice()   浅拷贝
var arr = [1, 3, { name: 'kobe', age: 10 }]
var arr2 = arr.slice()
arr2[2].name = 'wabe'
console.log(arr, arr2);







var str = '{"name":"小明","age":18}';
console.log(typeof str);

var json = JSON.parse(str) //要注意使用时是单引号套双引号
// var json = eval("(" + str + ")");
console.log(json);
console.log(typeof json);


// var j = eval("(" + str + ")")
// console.log(j);

// var st = (new Function("return" + str))();
// console.log(st);
// 深拷贝
// JSON.parse(JSON.stringify())
var arr = [1, 2, { name: 'kobe' }, function () { }]
var arr2 = JSON.parse(JSON.stringify(arr))
arr2[2].name = 'wabe'
console.log(arr, arr2);
// 注意：这样使用，得到的正则就不再是正则，得到的函数就不再是函数！！！！！！！！！！！！





var obj = { name: '张三', age: 12, prop: ['water', 'milk', 'orange', 'coffee'] }
// obj.a = obj;  // 栈溢出
// console.log(typeof obj);

function clone(obj) {
    if (typeof obj === "object") {
        var obj2 = Array.isArray(obj) ? [] : {}
        for (var key in obj) {
            obj2[key] = clone(obj[key])
        }
        return obj2;
    }else{
        return obj
    }

}
var res = clone(obj)
res.prop[1] = 'banana'
console.log(res);
console.log(obj);



