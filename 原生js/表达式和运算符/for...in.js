// for...in语句以任意顺序遍历一个对象的可枚举属性
var obj = {
    name:"李强",
    age:12,
    id:8,
    height:160,
    weight:66,
}
for(item in obj){
    console.log(item);
}

var arr = ['a','b','c','d']
for(item in arr){
    console.log(item);
}

// 不过更推荐在对象中使用for...in；在数组中使用for...of；


// for...in可以获取对象的属性名
// getOwnPropertyNames()同样也可以
var temp = {
    name:"王芳",
    age:12,
    id:0,
    weight:173,
    height:56,
}
console.log(Object.getOwnPropertyNames(temp));
// 在这里将得到的属性名组成一个数组