// 赋值运算符可以链式使用
var x = 23;
var y = 24;
var z = 25;

x = y;
console.log(x);
x = y = z;
console.log(x,y);