// do...while语句至少执行一次。

// do{

// } while()


var b = 2;
var result = 0;
do {
    result += b;
    b++;
}
while (b < 6)
console.log(result);
// result，2；b，3
// result，5；b，4
// result，9；b，5
// result，14；b，6



