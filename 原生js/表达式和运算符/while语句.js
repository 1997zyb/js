var s = 7;
while (s < 15) {
    s++;
    console.log(1);
}
console.log(s);
// 8，1
// 9，1
// 10，1
// 11，1
// 12，1
// 13，1
// 14，1
// 15，1

// while语句可以在某个条件为真的前提下，循环执行指定的一段代码，直到这个条件为false时结束。
var a = 6;
var b = 0;
while (a < 10) {
    a++
    b += a
    console.log(a);
}
console.log(b);
// a，7；b，7
// a，8；b，15
// a，9；b，24
// a，10；b，34