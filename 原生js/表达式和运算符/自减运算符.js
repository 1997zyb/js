// 有两种：x--和--x

// 如果操作数在前，则运算减一，返回减一之前的数
var x = 1;
if(true){
console.log(x--); // 1
}
console.log(x); //但是此时x的值位0，

var y = 2;
if(true){
    console.log(--y); // 1
}

// 总结：如果数字在前，就输出原来的；如果数字在后，就输出减去1的。

for(i = 0;i<10;++i){
    console.log(i)
}
for(i = 0;i<10;i++){
    console.log(i)
}