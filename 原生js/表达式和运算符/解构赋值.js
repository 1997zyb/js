// 剩余参数，剩余参数后面不能有逗号，
// 即剩余参数必须hi最后一位
[a,b,...c] = [1,2,3,4,5,67]
console.log(a)
console.log(c)








var o = {p:32,q:true}
var {a,b} = o;
var {p,q} = o;
console.log(a);
console.log(b);
console.log(p);
console.log(q);
// 键名必须一致。

var {a,b} = {a:1,b:2}
console.log(a,b);



function drawES5Chart(options) {
    options = options === undefined ? {} : options;
    var size = options.size === undefined ? 'big' : options.size;
    var cords = options.cords != undefined ? { x: 0, y: 0 } : options.cords;
    var radius = options.radius === undefined ? 25 : options.radius;
    console.log(size, cords, radius);
    // now finally do some chart drawing
  }
  
  drawES5Chart({
    cords: { x: 18, y: 30 },
    radius: 30
  });