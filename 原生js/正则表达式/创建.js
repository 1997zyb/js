var p = new RegExp("a")
console.log(p);
var s = /a/;
console.log(s);

var str = /helloWorld/
console.log(str.test("a"));
console.log(str.test("helloworldnihao"));

// 正则表达式
// I表示忽略大小写
// g表示全局匹配
// m表示多行匹配

// \d表示十进制的数字【0-9】，只要包含数字就是true，
// \D表示只要包含非数字就是true
// \s表示空白字符（空格，tab，回车键），只要有空白就是true，
// \S表示非空白符（空格，tab，回车键），只要有非空白就是true，
var p = /\s/
var str = "hell o"
console.log(p.test(str));

// \w表示一个字，0-9，a-z，A-Z，_
var p = /\w/
var str = "F3ng0b?"
console.log(p.test(str));
// \W表示除0-9，a-z，A-Z，_以外的字符，


// 原子，元字符，模式修饰符

// 开始元字符 ^，表示必须要以它后面紧贴的那个原子作为开始，
var p = /^\d/
var str = "abcdefg"
console.log(p.test(str));

// 结束元字符，表示必须要以它紧贴的那个原子作为结束
var p = /s$/
var str = "dfgdd"
console.log(p.test(str));

// 开始元字符与结束元字符一起使用
var p = /^[1]+[1,2,3,4,5,6,7,8,9,0]+\d{9}$/
var str = "18339696454"
console.log(p.test(str));


var p = /^jt/
var str = "jta"
console.log(p.test(str));



// 数量相关的元字符 {}，*，+，？
// {m}，表示前面的原子必须出现m次
// {m,n}，表示前面的原子至少出现m次，最多出现n次，

var p = /\d{2,3}/
var str = "1234567890"
console.log(p.test(str));


var p = /^a\d{2}/
var str = "a43"
console.log(p.test(str));


var p = /^a\d{2,4}$/ // 最多四个结束
var str = "a3567765"
console.log(p.test(str));


var p = /\d{4}-\d{1,2}-\d{1,2}/
var str = "你的生日是1989-01-01"
console.log(p.test(str));


// *前面的原子可以出现0次或很多次，
var p = /\w*/
var str = ",。/:|`"
console.log(p.test(str));

// +前面的原子至少出现1次，
var p = /\d+/
var str = "sfg4sfs"
console.log(p.test(str));

// ?前面的原子至少出现0次，或者1次，
var p = /\d?/
var str = "fghfhgh"
console.log(p.test(str));





var p =/^[1]+[34567890]+\d{9}$/
var str = "18339696454"
console.log(p.test(str));


var str = "3agfdg4hlpor"
var pat = /^[0-9]+/
console.log(pat.test(str));
// `^`表示以什么开头
// `+`表示匹配一个或多个
// [0-9]+表示匹配一个或多个数字
// `$`是结束标识符


// *号表示前面的字符可以不出现，也可以出现一次或多次
var str = "runoob"
var pat = /ro*/
console.log(pat.test(str));

// ？表示前面的字符最多出现一次
var str = "colocur"
var pat = /colou?/
console.log(str.match(pat));


// [ABC]表示匹配`[]`里面的所有字符
var str = "adggoghfsrlbmx"
var pat = /[ghdba]/
console.log(str.match(pat));
// 在使用match匹配时，不使用`g`表示没有使用全局匹配
// 打印出有四项
// （1）匹配到的字符串，
// （2）index，这个字符串的索引值
// （3）input，表示源字符串
// （4）groups：undefined，表示当前正则表达式没使用分组。


// [^ABC]表示匹配除了`[]`这个里面的字符。

