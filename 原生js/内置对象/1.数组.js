// 创建数组
var arr = new Array("apple","banana")
console.log(arr); // [ 'apple', 'banana' ]

var arr = new Array("apple")
console.log(arr); // ["apple"]

var arr = new Array(4)
console.log(arr);
console.log(arr.length); // 4

var arr = new Array(3,4)
console.log(arr);
console.log(typeof arr); // object
console.log(arr.length);

var arr = Array(4)
console.log(arr);
console.log(typeof arr); //object



var arr = [1,2,3,4,5,6]
arr.forEach((item,index,arr)=>{
console.log("索引为"+index +"对应的是" + item);
})

// 添加到末尾push，添加到最前面unshift
// 删除末尾元素pop，删除最前的元素shift
// 都会改变原数组


// 找出某个元素的索引 indexOf()
var arr = [1,2,3,4,5,6,"apple"]
console.log(arr.indexOf(4));
console.log(arr.indexOf("apple"));

// 通过索引删除某个元素，这个方法改变原数组
arr.splice(arr.indexOf(3),1)
console.log(arr);


// 复制一个数组
var arr = [1,2,3,4,5,6]
var arr2 = []
arr2 = [...arr]
console.log(arr2);

// concat不会改变原数组
var arr = [1,2,3,4,5,6]
var arr3 = []
console.log(arr3.concat(arr));

var arr = [1,2,3,4,5,6]
var arr4 = arr.slice(0)
console.log(arr4);

// 访问数组时，如果通过索引访问，索引无效时，只会返回undefined，并不会报错
// 访问以数字开头的属性时，只能通过中括号[]，即arr[0]


// join 把数组中的项以某种形式连接起来
// 结果是一个字符串
// join不会改变原数组

var arr = [1,2,3,4,5]
console.log(arr.join("-"));




// 数组实例
// 所有的数组实例都会从Array.prototype继承属性和方法，
console.log(Array.prototype.constructor == Array);




// 数组方法
// 1.Array.from()
// 从类数组或者可迭代对象中创建一个新的数组
// （拥有一个length属性回任若干索引属性的任意对象）

var str = "foo"
console.log(Array.from(str));

var set = new Set(['foo','bat','baz'])
console.log(set);
console.log(Array.from(set));


// 数组去重并合并
var m = [1,2,2],n = [2,3,3]
var mConcat = m.concat(n)
var resM = new Set(mConcat)
console.log(Array.from(resM));
// 注意：Array.from()只能用于类数组或者可迭代对象



// 2.Array.of()根据一组参数来创建新的数组，它不考虑参数的类型
console.log(Array.of(7));
var obj = {
    name:"李强",
    age:12,
    ifTrue:true,
    fun: function f(){}
}
console.log(Array.of(obj));
console.log(Array.of(undefined));


// 3.fill()用一个固定值填充一个数组中从起始索引到终止索引内的全部元素
// 包前不包后
var arr = [1,2,3,4,5]
arr.fill(1,3,"a","b")
console.log(arr.fill("a",1,4));





// toLocaleString()返回一个由所有数组元素组合而成的本地化后的字符串
var arr = ['a','b','c','d']
console.log(arr.toLocaleString());






// 2222222222222222
// 2222222222222222
// 2222222222222222
// 2222222222222222
// 2222222222222222


// 555555555
// 555555555
// 555555555
// 555555555
// 555555555
// 555555555
