// 所谓包装对象，指的是与数值、字符串、布尔值分别对应的Number、String、Boolean
// 三个原声对象。这三个原生对象可以把原始类型的值变为对象
var v1 = new Number(123);
var v2 = new String('strong');
var v3 = new Boolean(true)
console.log(typeof(v1,v2,v3));
// v1,v2,v3都是对象


// 2.实例方法
// 2.1 valueOf()返回包装对象实例对应的原始类型的值
var num = new Number(123).valueOf()
console.log(num);

var str = new String("strong").valueOf()
console.log(str);

var bool = new Boolean(true).valueOf()
console.log(bool);

// 2.2 toString()返回对应的字符串形式
var strNum = new Number(123).toString()
console.log(strNum);
console.log(strNum instanceof String);
console.log(typeof strNum);


// 3,原始类型与实例对象的自动转换
// 比如：字符串可以调用length属性
console.log("abcd".length);











