// JS的所有其他对象都继承Object对象,即那些对象都是Object的实例

// Object对象的原生方法分为:Object本身的方法和Object的实例方法

// 所谓  本身的方法  就是直接定义在Object对象的方法,

// 实例方法  就是定义在Object原型对象Object.prototype上的方法，
// 它可以被Object实例直接使用

Object.prototype.print = function () {
    console.log(",,,");
}
var object = new Object();
object.print()

// Object.keys()和Object.getOwnPropertyNames()方法都是用来遍历对象属性
// Object.keys()返回可枚举的
// Object.getOwnPropertyNames()返回不可枚举的,例如数组的length属性
var a =['a','b','c']
console.log(Object.getOwnPropertyNames(a));
console.log(Object.keys(a));



// Object本身是一个函数,可以当作工具方法使用,将任意值转为对象
// 如果参数是空(或者为undefined和null),Object()返回一个空对象
var object = Object();
console.log(typeof object);
// instanceof验证一个对象是否为指定的构造函数的实例
console.log(object instanceof Object);
 
// 如果Object方法的函数是一个对象,它总是返回该对象,即不用转换
var arr = [];
var obj = Object(arr)
console.log(obj);

// Object构造函数
var obj = new Object()


// Object的静态方法
// 所谓的静态方法,是指部署在Object对象自身的方法
// Object.keys()和Object.getOwnPropertyNames()方法都是用来遍历对象属性
// 区别:Object.getOwnPropertyNames()还能返回不可枚举的属性,例如length
// 这两个方法的参数是一个对象返回的是一个数组,是该对象的所有属性名

var obj = {
    name:"张三",
    age:"10",
    sex:"男"
}
console.log(Object.keys(obj));
console.log(Object.getOwnPropertyNames(obj));

// 其他方法
// (1)对象属性模型的方法
// Object.getOwnPropertyDescriptor()获取某个属性的描述对象
// Object.defineProperty()通过描述对象,定义某个属性
// Object.defineProperties()通过描述对象,定义多个属性

// (2)控制对象状态的方法
// Object.preventExtensions()防止对象扩展
// Object.isExtensible()判断对象是否可扩展
// Object.seal()禁止对象配置
// Object.isSealed()判断一个对象是否可配置
// Object.freeze()冻结一个对象
// Object.isFrozen()判断一个对象是否被冻结

// (3)原型链相关方法
// Object.create()该方法可以指定原型对象和属性.返回一个新对象
// Object.getPrototypeOf()获取对象的Prototype对象


// Object的实例方法
// Object.prototype.valueOf()返回当前对象对应的值
var obj = new Object();
console.log(obj.valueOf() === obj);

// toString()返回一个对象的字符串形式,默认情况下返回类型字符串
// 这个方法的应用:判断数据类型
var arr = [1,2,3,4]
console.log(Object.prototype.toString.call(arr));
console.log(new Date().toString());

// Object.prototype.hasOwnProperty()接受一个字符串作为参数,返回一个布尔值,表示该实例自身是否具有该属性
var obj = {
    p:123
}
console.log(obj.hasOwnProperty("p"));


// Object是一个构造函数

























