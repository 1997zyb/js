// String对象是JS原生提供的三个包装对象之一，用来生成字符串对象
var s1 = 'abc'
var s2 = new String('abc')
console.log(typeof s1);
console.log(typeof s2);
console.log(s2.valueOf());
// s1是字符串，s2是对象。由于s2是字符串对象，s2.valueOf方法返回它对应的原始字符串


// 字符串对象是一个类数组对象
var s3 = new String('abc')
console.log(s3.length);
console.log(s3[1]);



// 2.静态方法，即定义在对象本身，而不是定义在对象实例上

// 2.1 String.fromCharCode()   该方法的参数是一个或多个数值，代表Unicode码点，
// 返回值是这些码点组成的字符串
var Un = String.fromCharCode(97, 123, 85)
console.log(Un);


// 3.实例属性
// String.prototype.length返回字符串的长度
var res = 'milk'
console.log(res.length);

// charAt()方法返回指定位置的字符，参数是从0开始编码的位置
console.log(res.charAt(3));

// chartCodeAt()返回指定位置的Unicode码点
console.log(res.charCodeAt(3));



// String.prototype.concat()方法用于连接两个字符串，返回一个新字符串，不改变原字符串
var one = '1';
var two = '2';
var three = '3';
console.log(one.concat(two, three));


// String.prototype.slice()方法用于从原字符串取出字符并返回
var Js = 'JavaScript'
console.log(Js.slice(0, 4));

// substring()、substr()和slice()用法一样



// String.prototype.indexOf()方法用于确定第一次出现指定字符的位置
var str = "JavaScript"
console.log(str.indexOf('a'));
console.log(str.lastIndexOf('a'));
// lastIndexOf()用于确定最后一次出现该字符的位置

// String.prototype.trim()方法用于去除字符串两端的空格，返回一个新字符串，不改变原字符串
var res = " hello "
console.log(res.trim());
// 该方法不仅可以去除空格，还包括制表符(\t、\v)、换行符(\n)和回车符(\r)
var res = "\thello"
console.log(res.trim());


// String.prototypr.toLowerCase()将一个字符串全部转为小写
// String.prototypr.toUpperCase()将一个字符串全部转为大写
var lower = 'Milk'
console.log(lower.toLocaleLowerCase());
console.log(lower.toLocaleUpperCase());


// String.prototype.match()方法用于确定原字符是否匹配到某个子字符串
// 返回一个数组，成员为匹配的第一个字符串，如果没有找到，返回null
var s = "'water','orange','coffee','milk'"
console.log(s.match('orange'));

// search方法的用法基本等同于match,但是返回值为匹配的第一个位置。如果没有找到，则返回-1
console.log(s.search('o'));

// replace方法用于替换匹配的子字符串，一般情况下只替换第一个匹配
var res = 'apple'
console.log(res.replace('p', 'b'));


// String.prototypr.split()方法按照给定规则分割字符串，
// 返回一个由分割出来的子字符串组成的数组
var app = 'milk,orange'
console.log(app.split(""));
console.log(app.split(" "));
console.log(app.split());
console.log(app.split(","));





var s = new String("hello")
console.log(typeof s);
console.log(s);
console.log(s.toString());
console.log(Object.prototype.toString(s));
// 复合类型的数据，使用typeof时，只有function返回的时functino，其他返回object。
// String是原生函数，可以被当作构造函数来使用，即 new String()

var arr = [1, 2, 3, 4]
console.log(typeof arr);
console.log(Object.prototype.toString(arr));
var stu = {
    name: "李强",
    age: 12,
}
console.log(typeof stu);

function g(){

}
console.log(typeof g);

var arr1 = new Array(1,2,3)
console.log(arr1);
console.log(typeof arr1);
console.log(arr1.valueOf());

var str = new String("abc")
console.log(str);
console.log(str.valueOf());




















