// Number对象是数值对应的包装对象，可以作为构造函数使用，也可以作为工具函数使用

// 作为构造函数，它用于生成值为数值的对象
var num = new Number(12);

// 作为工具函数时，将任何类型的值转为数值
console.log(Number(null));




// 1.实例方法   Number对象有4个实例方法，
// 1.1 Number.prototype.toString()用来将一个数值转为字符串形式

var a = 10;
var strNum =a.toString(2)
console.log(strNum);
// toString方法接受一个参数，表示输出的进制，如果省略，默认转为十进制


// 1.2Number.prototype.toFixed()
// toFixed方法先将一个数转为指定位数的小数，然后返回这个小鼠对应的字符串
var b = 3.1415
var result = b.toFixed(2)
console.log(result);



//3.自定义方法
// Number.prototype对象上可以自定义方法，被Number的实例继承 
var a = 8;
Number.prototype.iterate = function(){
    var result = [];
    for (var i=0;i<=this;i++){
        result.push(i);
    }
    return result;
}
var res = a.iterate()
console.log(res);







































