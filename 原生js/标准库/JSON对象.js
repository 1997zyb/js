// JSON格式是一种用于数据交换的文本格式，目的是取代繁琐笨重的XML格式


// JSON.stringify()用于将一个值转为JSON字符串。
var s = 123;
var res = JSON.stringify(s)
console.log(res);
console.log(typeof s);
console.log(typeof res);

console.log(JSON.stringify({name:'张三'}));




// JSON.parse方法用于将JSON字符串转换为对应的值
var o = JSON.parse('{"name":"张三"}')
console.log(o);
console.log(o.name);















