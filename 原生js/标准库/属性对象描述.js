// JS提供了一个内部数据结构,用来描述对象的属性,控制它的行为,
// 比如该属性是否可写,可遍历等,这个内部数据结构称为 "属性描述对象"
{
    value:123;//value是该属性的属性值,默认为undefined
    
    writable:false;//writable是一个布尔值,表示属性值value是否可变,默认为true
    
    enumerable:true;//enumerable是一个布尔值,表示该属性是否可遍历,默认为true
    
    configurable:false;//configurable是一个布尔值,表示可配置,默认为true
    
    get:undefined;
    
    set:underfined;
}





// 1.Object.getOwnPropertyDescriptor()方法可以获取属性描述对象
// 第一个参数是目标对象,第二个参数是一个字符串,对应目标对象的某个属性名
// 这个方法只能用于对象自身的属性,不能用于继承的属性
var obj = {name:"张三"}
var des = Object.getOwnPropertyDescriptor(obj,'name');
console.log(des);
console.log(Object.keys(obj));


// 2.Object.getOwnPropertyNames()返回一个数组
var obj = Object.defineProperties({},{
    p1:{name:'Z3',age:10,enumerable:true},
    p2:{name:'L4',age:8,enumerable:false}
})
console.log(Object.getOwnPropertyNames(obj));
console.log(Object.keys(obj));//p2是不可遍历的



































