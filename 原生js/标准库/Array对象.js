// Array是JS的原生对象，同时也是一个构造函数，可以用它生成一个新数组

// Array构造函数有一个很大缺陷，参数不一样时，会导致他的行为不一致
// 无参数时，返回一个空数组
console.log(new Array());

// 有一个参数时，表示返回的是数组的长度
var arr = Array(2)
console.log(arr);

// 多个参数时，返回的是新数组的成员
var arr = Array(1,2,3,4)
console.log(arr);
var arr = Array('abc')
var as = Array([1])


// 直接使用数组字面量是更好的做法
var arr = [1,2,3]
for(a in arr){
    console.log(a); 
}
var stu = {
    name:'张三',
    age:12,
    sex:'男',
}
for(s in stu){
    console.log(s);   
}

// 静态方法
// Array.isArray()方法返回一个布尔值，表示参数是否数组，他可以弥补typeof的不足
var arr = ['a','b','c']
console.log(typeof arr); //object
console.log(Array.isArray(arr)); //true
// typeof运算符只能显示数组的类型是Object,Array.isArray()方法可以识别数组

// 实例方法
// valueOf()   toString()
var arr = [1,2,3]
console.log(arr.valueOf());

// toString方法也是对象的通用方法，数组的toString方法返回数组的字符串形式
var arr = ['apple',['orange',['banana','milk']]]
console.log(arr.toString());

// push方法用于在数组的末端添加一个或多个元素，并返回添加后的新数组，该方法会改变原数组
var arr = [];
arr.push(1);
arr.push('cb');
arr.push(true)
console.log(arr);
// unshift方法用于在数组的第一个位置添加元素，并返回添加新元素后的数组长度，该方法会改变数组
// 这个方法可以接收多个参数，这些都可以添加到头部
var a = ['apple','banana','blue']
a.unshift('water')
console.log(a);


// pop方法用于删除数组的最后一个元素，并返回该元素，该方法会改变原数组
var arr = ['red','black','purple']
arr.pop()
console.log(arr);

// shift方法删除数组的第一个元素，并返回该元素，该方法会改变原数组
var arr = ['a','b','c','d']
arr.shift()
console.log(arr);


// 1.join()方法以指定参数作为分隔符，将所有数组成员连接为一个字符串返回。
// 如果不提供参数，默认用逗号分隔；
var a = [1,2,3,4]
console.log(a.join());
console.log(a.join('|'));
console.log(a.join(' '));

// 通过call方法，这个方法也可以用于字符串或类数组的对象
var obj = {0:'a',1:'b',2:'c',length:3}
console.log(Array.prototype.join.call(obj,'-'));

// 2.concat()方法用于多个数组的合并。它将新数组的成员，添加到原数组成员后面
// 并返回一个新数组，原元素组不变
var a = ['apple','blue']
var b = [['red'],'purple']
var conc = a.concat(b)
console.log(conc);
b[0][0]='black'
console.log(conc);

// 如果数组成员包括对象，concat方法返回当前数组的一个浅拷贝
var obj = [{a:1}];
var newObj = obj.concat()
console.log(newObj);
obj[0].a = 6;
console.log(newObj);
// 上面代码中，原数组中包含一个对象，改变原数组，新数组也改变



// 3.reverse()方法用于颠倒排列数组元素，返回改变后的数组。该方法会改变原数组
var a = ['water','orange','coffee','milk']
var aReverse = a.reverse()
console.log(aReverse);

// 4.slice()方法用于提取目标数组的一部分，返回一个新数组，原数组不变
var arr = ['apple','coca','milk','water','coffee','food']
var b = 'www.baidu#123/useId=1'
console.log(arr.slice(0));
console.log(arr.slice(0,2));
console.log(b.slice(14));
// 注意：如果第一个参数大于第二个参数，或者第一个参数小于数组长度，则返回空数组

// slice另一个重要应用就是将类数组转化为数组
var arrLike = {0:'a',1:'b',2:'c',length:3}
var arr = Array.prototype.slice.call(arrLike)
console.log(arr);


// 5.splice()方法用于删除原数组的一部分成员，并可以在删除的位置添加新的数组成员
// 返回值是被删除的数组。该方法会改变原数组
// splice()第一个参数是删除的起始位置，第二个是被删元素的个数，
// 如果后面还有参数，则表示这些就是要被插入数组的新数组
var a = ['a','b','c','d','e']
// console.log(a.splice(0));
// console.log(a);
// console.log(a.splice(1,2));
// console.log(a);
console.log(a.splice(2,1,'w','x'));
console.log(a);
// 如果起始位置是负数，就代表从倒数位置开始删除

var a = [1,2,3,4]
console.log(a.slice(0,2));

console.log(a.splice(2));
console.log(a);


// 6.sort()方法对数组成员进行排序，默认是按字典顺序排序，排序后，原数组将改变
var a = ['d','z','a','c']
console.log(a.sort());

// 如果让sort方法按自定义方式排序，可以传入一个函数作为参数
var people = [
    {name:"张三",age:10},
    {name:"李四",age:6},
    {name:"王五",age:7},
]
// 如果a-b大于0，表示a在b后面
let result = people.sort(function(a,b){
    return a.age-b.age;
})
console.log(result);

// 7.map()方法接受一个函数作为参数。该函数调用时，
// map方法向它传入三个参数：当前成员，当前位置和数组本身
var arr = [1,2,3]
let arrResult = arr.map(function(item){
    return item*item;
})
console.log(arrResult);

// map方法还可以接受第二个参数，用来绑定回调函数内部的this变量，？？？？？？
var arr = ['a','b','c'];
[1,2].map(function(item){
    // arr是第二个参数，结果回调函数内部关键字this就指向arr
    console.log(this[item]);   
},arr)


// 8.forEach()方法与map()方法类似，也是对数组成员依次执行参数函数。
// forEach方法不返回值，只是来操作数据。
// 这就是说，如果数组遍历的目的是为了得到返回值，用map ;否则用forEach


// forEach的用法与map一致，参数是一个函数，该函同样接受三个参数：当前值；当前位置；整个数组

var arr = [2,5,7,3]
arr.forEach(function(item,index,arr){
   console.log('[' + index + '] = ' + item);
})

// forEach方法也可以接受第二个参数，绑定参数函数的this变量
// 注意：forEach方法无法中断执行，总是会将所有成员遍历完。
// 如果希望符合某个条件时中断执行，用for循环
var arr = [1,2,3]
for(i = 0;i < arr.length;i++){
    if(arr[i] === 2) break;
    console.log(arr[i]);
}


// 9.filter方法用于过滤数组成员，满足条件的成员组成一个新数组返回。
// 它的参数是一个函数，该方法不会改变原数组
var arr = [1,2,3,4,5]
var s = arr.filter(function(item,index,arr){
    return (item % 2 ===0 )
})
console.log(s);
// filter方法的参数可以接受三个参数：当前成员；当前位置；和整个数组
// filter方法还可以接受第二个参数，用来绑定参数内部的this变量


// 10.some()方法只要一个成员的返回值是true，则整个some方法的返回值就为true，否则为false
var arr = [1,2,3,4,5]
var s = arr.some(function(item,index,arr){
    return item > 2
})
console.log(s);

// 11.every()方法是所有的成员返回值都是true，整个every方法才返回true



// 12.reduce()和reduceRight()都是依次处理数组成员，最终累计为一个值。
// 区别：reduce()是从左往右计算；reduceRight()是从右往左计算

var a = [1,2,3,4,5,6]
var s = a.reduce(function(m,n){
    console.log(m,n);
    return m+n;
})
console.log(s);


var a = ['aaaa','bb','xxxxxxxx']
function f(a){
    return a.reduce(function(m,n){
        // console.log(m,n);
        return m.length > n.length ? m : n
    })    
}
console.log(f(a));



// 13.indexOf()方法返回给定元素在数组第一次出现的位置，如果没有出现则返回-1；
// indexOf()方法还可以接受第二个参数，表示搜索开始的位置
// lastIndexOf()方法返回给定元素在数组中最后一次出现的位置，如果没有出现则返回-1
var a = [2,4,3,2,1,4,5,6,8,5]
console.log(a.indexOf(5));
console.log(a.lastIndexOf(4));


































































