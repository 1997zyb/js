// Boolean函数的类型转换作用
console.log(Boolean(null));
console.log(Boolean(undefined));
console.log(Boolean(NaN));
console.log(Boolean(''));
console.log(Boolean(0));
// 以上情况都是 “false”


console.log(Boolean(1));
console.log(Boolean('false'));
console.log(Boolean([]));
console.log(Boolean({}));
console.log(Boolean(function(){}));
console.log(Boolean(/foo/));
// 以上情况情况都是 “true”






