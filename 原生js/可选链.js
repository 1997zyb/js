var obj = {
  stu: {
    name: "李强"
  }
}
console.log(obj.stu.name);
console.log(obj?.stu?.name);



let res = {
  code: 200,
  data: {
    //   content: [1,2,3], //table list
    page: 1
  },
  message: 'success',
  func: function () { console.log('I am func') }
}
const tableList = res.data && res.data.content; //[1,2,3]

console.log(tableList);
// 相当于
const tableList = res.data ? res.data.content : undefined;
// 有了可选链, 简写为
const tableList = res.data?.content  //[1,2,3]

// 甚至更谨慎点，加长判断，多个三目运算
const tableList = res && res.data && res.data.content;
// 有了可选链, 简写为
const tableList = res?.data?.content  //[1,2,3]



var arr = [
  { name: "李强", age: 12, sex: "男", id: 1 },
  { name: "王洪", age: 14, sex: "男", id: 2 },
  { name: "李玉", age: 11, sex: "男", id: [1,2,3,4] },
  6, 7
]
if(arr?.indexOf(6) != -1){
  console.log("可选链");
}