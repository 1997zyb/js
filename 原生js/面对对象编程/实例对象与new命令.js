// new命令的原理（new干了什么）
// （1）创建一个空对象，作为将要返回的对象实例
// （2）将这个空对象的原型，指向构造函数的prototype属性？？？？？？？？？？？？？？
// （3）将这个空对象赋值给函数内部的this关键字
// （4）开始执行构造函数内部的代码


// new.target
// 函数内部可以使用new.target属性。、
// 如果当前函数是new命令调用，new.target指向当前函数，否则为undefined
function f(){
    console.log(new.target === f);
}
f()
new f()
// 所以使用这个属性，可以判断函数调用的时候，是否使用了new命令




// 4.Object.create()创建实例对象
// 构造函数可以作为模板，生成实例对象，但是有时拿不到构造函数，只能拿到一个现有对象
// 我们希望以这个现有对象作为模板，生成新的实例对象。
var person1 = {
    name:'张三',
    age:12,
    greeting(){
        console.log('Hi!,my name is ' + this.name);
    }
}
var person2 = Object.create(person1)
console.log(person2.name);
person2.greeting()









// new命令
// new命令的作用,就是执行构造函数,返回一个实例对象
var V = function () {
    this.price = 1000;
}
var v = new V();
console.log(v.price);




