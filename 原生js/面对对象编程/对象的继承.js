function Animal(name){
    this.name = name;
}
Animal.prototype.color = 'white',
Animal.prototype.walk = function(){
    console.log('走路');
}
var cat1 = new Animal('大毛');
var cat2 = new Animal('小毛');
console.log(cat1.meow === cat2.meow);


function Cat(name,color){
    this.name = name;
    this.color = color;
    this.meow = function(){
        console.log('喵');
    }
}
var cat1 = new Cat('大毛','白色')
var cat2 = new Cat('大毛','白色')
console.log(cat1.meow === cat2.meow);
// 这种的缺点就是：多个实例之间无法共享属性，每生成一个实例，就会生成一个属性


// 1.2prototype属性的作用
// 所有的方法和属性定义在原型上，那么所有实例对象就能共享
function Animal(name){
    this.name = name;
}
Amimal.prototype.color = 'white'
var cat1 = new Amimal('大毛');
var cat2 = new Amimal('二毛');
console.log(cat1.color);
console.log(cat2.color);
// 上述代码中，构造函数Animal的prototype属性，就是实例对象cat1和cat2的原型对象

// 当实例对象本身没有某个属性或方法时，它会到原型对象去寻找该属性或方法！！！！！！！！！！！！！
// 如果实例对象自身就有某个属性或方法时，它不会去原型对象上寻找

// 总结：原型的作用，就是定义所有实例对象共享的属性和方法。！！！！！！！！！！！！！！！！


// 1.3原型链
// 所有对象都有自己的原型对象。
// 一方面，任何一个对象，都可以充当其他对象的原型，
// 另一方面，由于原型对象也是对象，所以他也有自己的原型。因此会形成一个‘原型链’
// 如果一层一层追溯，所有对象的原型都可以追溯到Object.prototype,即Object构造函数的prototype属性

// Object.prototype对象有没有它的原型？答：它的原型是null

var MyArray = function(){

}
MyArray.prototype = new Array()
// 表示MyArray.prototype指向一个数组实例
// 所以可以使用数组方法
var mine = new MyArray();
mine.push(1,2,3);
console.log(mine.length);



// 1.4 constructor属性
// prototype对象上有一个constroctor属性，默认指向prototype对象所在的构造函数。
function P(){

}
console.log(P.prototype.constructor === P);
// 由于constructor属性定义在prototype上，所以实例对象也可以使用
var p = new P()
console.log(p.constructor === P);
// constructor属性可以得知某个实例对象，到底是哪一个构造函数产生的 
// 可以从一个实例对象，新建另一个实例对象
function Constr(){

}
var x = new Constr()
var y = new x.constructor()
console.log(y instanceof Constr);




// 2.instanceof运算符返回一个布尔值，表示对象是否为某个构造函数的实例
function Vehicle(){

}
var v = new Vehicle();
console.log(v instanceof Vehicle);
// instanceof运算符只能用于对象，不能用于原始类型的值



// 3. 构造函数的继承














