// 1.Object.getPrototypeOf()方法返回参数对象的原型。
var F = function () {

}
var f = new F()
console.log(Object.getPrototypeOf(f) === F.prototype);


// 2.Object.setPrototypeOf()方法为参数对象设置原型，返回该参数对象
// 它接收两个对象，一个是现有对象，另一个是原型对象
var a = {}
var b = { x: 1 };
Object.setPrototypeOf(a, b)
console.log(Object.getPrototypeOf(a));
console.log(Object.getPrototypeOf(a) === b);
console.log(a.x);


// Object.create()
// 该方法接受一个对象作为参数，然后以它为原型,返回一个实例对象
var A = {
    print: function () {
        console.log("hello");
    }
}
var B = Object.create(A)
// B.print()
// Object.create方法以A对象为原型,生成B对象,B继承了A的所有属性和方法

// 除了对象原型,Object.create方法还可以接收第二个参数,该参数是一个属性描述对象
// 它所描述的对象属性,会添加到实例对象,作为该对象自身的属性
var obj = Object.create({}, {
    p1: {
        value: 123,
        enumerable: true,
        configurable: true,
        writable: true,
    },
    p2: {
        value: [4,5,6],
        enumerable: true,
        configurable: true,
        writable: true,
    },
})


// 4.Object.prototype.isPrototypeOf()用来判断该对象是否为参数对象的原型
var o1 = {};
var o2 = Object.create(o1);
var o3 = Object.create(o2);
console.log(o1.isPrototypeOf(o2));



// 5.Object.prototype.__proto__返回该对象的原型,该属性可读写
var obj = {};
var p = {};
obj.__proto__ = p;
console.log(Object.getPrototypeOf(obj) === p);
// 上面代码通过__proto__属性,将p对象设为obj对象的原型

var A = {
    name:'张三'
};
var B = {
    name:'李四'
};
var proto = {
    print:function(){
        console.log(this.name);
    }
}
A.__proto__ = proto;
B.__proto__ = proto;
A.print()
// A对象和B对象的原型都是proto对象,它们共享proto对象的print方法




// 7.Object.getOwnPropertyNames()获取参数对象的所有属性,返回的是一个数组
// 可不可遍历的属性名都会返回
var p = {
    name:'张三',
    age:12,
    sex:'男'
}
console.log(Object.getOwnPropertyNames(p));


// 8.Object.prototype.hasOwnProperty()方法返回一个布尔值,
// 用于判断某个属性定义在对象自身,还是定义在原型链上
console.log(p.hasOwnProperty('name'));



// 9.in运算符和for...in循环
// in运算符返回一个布尔值,表示一个对象是否就有某个属性,不区分该属性是对象自身属性,还是继承属性

console.log('name' in p);



// 获得对象的所有可遍历(不管是自身还是继承的),可以使用for...in循环
var o1 = {p1:123};
var o2 = Object.create(o1,{
    p2:{value:'milk',enumerable:true}
})
for(value in o2){
    console.log(value);
}
console.log(Object.getOwnPropertyNames(o2));
console.log(o2.hasOwnProperty(p1));


// 10.对象的拷贝
















