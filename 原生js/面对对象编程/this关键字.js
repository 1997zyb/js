// this可以用在构造函数中，表示实例对象，

// this都有一个共同点：它总是返回一个对象


var person = {
    name: '张三',
    desc() {
        return '姓名：' + this.name
    }
}
console.log(person.desc());
// this.name表示name属性所在的那个对象，由于this.name是在desc方法中调用，
// 而desc方法所在的当前对象是person，因此this指向person，this.name就是person.name 



// 由于对象的属性可以赋给另一个对象，所以属性所在的当前对象是可变的，
// 即this的指向是可变的
var A = {
    name: '张三',
    des: function () {
        return 'My name is ' + this.name
    }
}
var B = {
    name: '李四'
}
B.des = A.des;
console.log(B.des());
// 上面代码中，A.des属性被赋给B，于是B也有des这个方法



var A = {
    name: '张三',
    des: function () {
        return '姓名：' + this.name
    }
}
var name = '李四';
var f = A.des;
console.log(f());
// this就是指函数运行时所在的对象（环境）



// 3.使用场合
// （1）全局环境    它指的就是顶层对象window
// （2）构造函数    构造函数中的this，指的就是实例对象
// （3）对象的方法  如果对象的方法里面包含this，this的指向就是方法运行时所在的对象。
// 该方法赋值给另一个对象，就会改变this指向


// 如果this所在的方法不在对象的第一层，这时this只是指向当前一层的对象，
var a = {
    p: 'hello',
    b: {
        m: function () {
            console.log(this.p);
            // 此时，this就指向window
            console.log(this);
        }
    }
}
// a.b.m()
var msg = a.b.m
// 如果这时将嵌套对象内部的方法赋给一个变量，this依然会指向全局变量
msg()

// 4.避免多层使用this
var o = {
    f1: function () {
        console.log(this === o);
        var f2 = function () {
            console.log(this);
        }()
    }
}
o.f1()



// 在第二层改用一个指向外层this的变量
var o = {
    f1: function () {
        console.log(this);
        var that = this;
        var f2 = function () {
            console.log(that);
        }()
    }
}
o.f1()



// 5.绑定this的方法

// call的第一个参数就是this所要指向的那个对象，后面的参数则是函数调用时所需的参数
// 格式为：func.call(obj,参数一，参数二，参数三，...)
var obj = {
    name: '李四',
    age: 1,
}
var f = function () {
    return this;
}
console.log(f());
console.log(f.call(obj));

// apply方法的作用与call方法类似 ，也是改变this指向，
// 唯一区别是：它接收一个数组作为函数执行时的参数，
// func.apply(obj,[参数一，参数二，参数三，...])










// this就是属性或方法‘当前’所在的对象





foo(); //1

var foo;

function foo() {
    console.log(1);
}

foo = function () {
    console.log(2);
}






f()
function f() {
    console.log('2');
}
var f = function () {
    console.log('1');
}
