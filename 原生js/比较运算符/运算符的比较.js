// > 大于运算符

// < 小于运算符

// <= 小于或等于运算符

// >= 大于或等于运算符

// == 相等运算符

// === 严格相等运算符

// != 不相等运算符

// !== 严格不相等运算符

console.log(2 > 1);



// 字符串的比较是根据字典顺序（实际是Unicode码点）
console.log('a' > 'b');
console.log('张' > '亚');

// NaN与任何值比较返回都是false，包括它本身
console.log(100 > 'z'); // false
console.log(100 > '10'); // true
console.log(100 > NaN); // false

console.log(Number(true)); // 1


var x = [2]
var y = x.toString()
console.log(typeof y);
console.log(x.valueOf());
console.log(x.valueOf().toString());

// 如果是对象，先转化为原始类型的值
// 则调用valueOf().toString()方法
console.log(x > 11);





// 严格相等运算符 ===
// NaN与任何值都不相等，包括自身
// +0和-0相等
console.log(1 === '1');
console.log(1 == '1');
console.log(NaN == NaN);
console.log(NaN === NaN);
console.log(+0 == -0);
console.log(+0 === -0);


// 复合类型的值比较，比较他们是否指向同一地址
var a = function(){

}
var b = function(){

}
console.log(a === b);

var a = b;
console.log(a === b);

console.log({} === {});




// undefiend和null与自身严格相等
console.log(undefined === undefined);
console.log(null === null);


// !== 严格不相等运算符
// 就是先求严格相等运算符的值，然后返回相反值
console.log(1 !== '1');






// 相等运算符 ==
console.log(1 == 1.0);

// 原始类型的值会先转成数值再进行比较
console.log('张' == true);
console.log('张' == 0);
console.log(Number('张'));
console.log(Number('asd'));

// 对象与原始类型值比较
// 先转化为原始类型的值，在进行比较
console.log([1] == 1);
console.log(Number([1]) == 1);

console.log([1] == '1');
console.log(String([1]) == '1');



// undefined和null与其他值比较时，结果都是false，
// 他们互相比较时结果为true
console.log(undefined == '');
console.log(undefined == null);





// 不相等运算符，
// 先求相等运算符的结果，再返回相反值，





























console.log(5 > 'abc');
console.log(5 > true);
console.log(5 > '3');

console.log(1 > NaN);
console.log(NaN > NaN);
console.log('1' > NaN);
console.log(1 >= NaN);
console.log(1 < NaN);
console.log(1 <= NaN);
console.log(NaN <= NaN);

var x = [2]
console.log(x.valueOf());
console.log(x.toString());
console.log(x > '11');
console.log('2' > '11');
console.log('5' > '4');






console.log('5' > '11');






var a = function(){
    x = 1;
}
var b = a
console.log(a === b);
console.log(1 === 0x1);
console.log(15 === 0xF);


console.log(1 !== '1'); // true
console.log([] !== []); // true

console.log(1 == 1.0); // true

console.log(undefined === undefined); // true
console.log(null === null); // true

var a = function(){
    x = 1;
}
var b = function(){
    x = 1;
}
console.log(a == b); // false





console.log(1 == true); //true
console.log(1 == '1'); //true
console.log('1' == '1'); //true
console.log(undefined == undefined);
console.log(null == null);
console.log(undefined == null);