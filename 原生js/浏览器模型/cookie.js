// Cookie是服务器保存在浏览器的一小段文本信息,一般大小不超过4KB
// 浏览器每次向服务器发送请求,就会自动带上这段信息

// 对话管理:保存信息,购物车
// Cookie不是一种理想的客户端存储机制,容量很小(4KB),缺乏数据操作接口,而且影响性能
// 客户端存储应该使用Web storage API和IndexedDB,
// 只有那些每次请求都需要让服务器知道的信息,采访在Cookie里面

    // 每个Cookie都有以下几方面的元数据
    // Cookie的名字
    // Cookie的值(真正的数据写在这里面)
    // 到期时间(超过这个时间会失效)
    // 所属域名(默认为当前域名)
    // 生效的路径(默认为当前网址)

// 不同浏览器对Cookie数量和大小的限制是不一样的,
// 一般来说单个域名设置的Cookie数量应不超过30个,大小不超过4KB




// 2. Cookie与HTTP协议      Cookie由HTTP协议生成,也主要供HTTP协议使用
// 服务器如果希望在浏览器内保存Cookie,就要在HTTP回应的头信息里面,放置一个Set-Cookie
// 除了Cookie的值,Set-Cookie字段还可以附加Cookie的属性



// 3.Cookie的属性

// Expires,(指定到了某个时间,浏览器就不再保留这个Cookie)
// Max-Age(优先生效)(指定从现在开始Cookie存在的秒数)

// Domain属性指定浏览器发出HTTP请求时,哪些域名要带上这个Cookie
// Path属性指定浏览器发出HTTP请求时,哪些路径要带上这个Cookie



// 4.document.cookie用于读取当前网页的Cookie
// 这个属性可写的,可以通过它为当前网站添加cookie
// 等号两边不能有空格
document.cookie = 'key=value'
document.cookie = 'foo=bar;Max-age=2'

// 删除一个现存Cookie的唯一方法,是设置它的expires属性为一个过去的日期








