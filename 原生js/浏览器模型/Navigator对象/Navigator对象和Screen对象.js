Navigator属性指向一个包含浏览器和系统信息的Navigator对象
脚本通过这个属性了解用户的环境信息

navigator.userAgent属性返回浏览器的 User Agent字符串,表示浏览器的厂商和版本信息

navigator.plugins属性返回一个类数组对象,成员是Plugin实例对象,表似乎浏览器安装的插件

navigator.platform属性返回用户的操作系统信息
platform: "Win32"


navigator.language属性返回一个字符串,表示浏览器的首选语言,该属性只读
language: "zh-CN"

navigator.languages表示用户可以接受的语言



navigator.cookieEnabled属性返回一个布尔值,表示浏览器的Cookie功能是否打开
cookieEnabled: true



2.Navigator对象的方法
navigator.javaEnabled()方法返回一个布尔值,表示浏览器能否运行Java小程序





3.Screen对象
Screen对象表示当前窗口所在的屏幕,提供显示设备的信息
window.screen








