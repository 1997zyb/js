// Storage接口用于脚本在浏览器保存数据,两个对象部署了这个接口:
    // window.sessionStorage和window.localStorage


// sessionStorage保存的数据用于浏览器的一次会话,当会话结束后(通常是浏览器关闭),数据被清空;
// localStorage保存的数据长期存在,下一次访问该网站时,网页可以直接读取数据
// 保存的数据都是以'键值对'的形式存在



// 2.1 Storage.setItem()
// 用于存入数据,接收两个参数,第一个参数时键名,第二个参数是保存的数据;如果键名已存在,则该方法会更新已有的键值
// 例:window.sessionStorage.setItem('name','张三')

// 2.2 Storage.getItem()
//用于读取数据.它只有一个参数,就是键名 
// 例:window.sessionStorage.getItem('name')

// 2.3 Storage.removeItem()
// 用于清除某个键名对应的键值,它接受键名作为参数
// 例:window.sessionStorage.removeItem('name')

// 2.4 Storage.clear()
// 用于清除所有保存的数据




// 2.5 Storage.key()
// 接受一个整数作为参数(从零开始),返回该位置对应的键值
