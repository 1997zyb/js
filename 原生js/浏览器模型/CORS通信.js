// CORS是一个W3C标准，全称是“跨域资源共享”。它允许浏览器像跨域的服务器，发送AJAX请求


// 整个CORS通信过程，都是浏览器自动完成，不需要用户参与。
// 浏览器一旦发现AJAX请求跨域，就会自动添加一些附加的头信息，有时还会多出现一次附加的请求
// 但是用户不会有感知。因此，实现CORS通信关键是服务器
// 只要服务器实现了CORS接口，就可以跨域通信。




// 二、两种请求
    // CORS请求分为：简单请求和非简单请求

// 只要同时满足一下两大条件，就是简单请求
    // （1）请求方法是以下三种方法之一。
        // HEAD   GET   POST
    
    // HTTP的头信息不超过以下几种字段
        // Accept
        // Accept-Language
        // Content-Language
        // Last-Event-ID
        // Content-Type




// 三、简单请求

// 3.1基本流程
// 对于简单请求，浏览器直接发出CORS请求。具体来说，就是在头信息之中，增加一个Origin字段
// Origin用来说明本次请求来自哪个域（协议+域名+端口）

// 如果Origin指定的源在许可范围内，服务器返回的响应，会多出几个头信息字段

// Access-Control-Allow-Origin
// 该字段是必须的，它的值要么是请求时Origin字段的值，要么是一个 * 

// Access-Control-Allow-Credentials
// 该字段可选，表示是否允许发送Cookie,默认不可以

// Access-Control-Allow-Credentials: true
// 还必须打开AJAX的withCredentials = true;

// Access-Control-Expose-Headers
// 该字段可选。CORS 请求时，XMLHttpRequest对象的getResponseHeader()方法只能拿到6个服务器
// 返回的基本字段：
// Cache-Control、Content-Language、Content-Type、Expires、Last-Modified、Pragma。
// 如果想拿到其他字段，就必须在Access-Control-Expose-Headers里面指定。







// 4.非简单请求
    // 4.1预检请求
        // 非简单请求的CORS请求，会在正式请求前，增加一次HTTP查询请求
        // 1.当前网页的域名是否在服务器的许可名单中
        // 2.使用了那些HTTP方法和头信息

// “预检”请求用的请求方法是OPTIONS，表示这个请求是用来询问的，
// 头信息里面关键字段是Origin，表示请求来自哪个源

// （1）Access-Control-Request-Method
// 该字段是必须的，用来列出浏览器的CORS请求会用到哪些HTTP方法

// （2）Access-Control-Request-Headers
// 该字段是一个用逗号分隔的字符串,指定浏览器CORS请求会额外发送的头信息字段



    // 4.2预检请求的回应
    // 服务器收到"预检"请求后,检查了Origin,Access-Control-Request-Method,Access-Control-Request-Headers
    // 确认允许跨域后就可以做出回应

    // 里面包含的字段有:
    // Access-Control-Origin:*;同意任意跨域请求
    // Access-Control-Allow-Methods:GET,POST,PUT;表示服务器支持的所有跨域请求方法

    // Access-Control-Allow-Headers

    // Access-Control-Allow-Credentials

    // Access-Control-Max-Age,用来指定本次预检请求的有效期,单位是秒,





    // 4.3浏览器的正常请求和回应
// 一旦服务器通过了"预检"请求,以后每次浏览器正常的CORS请求,就跟简单请求一样,
// 会有一个Origin头信息字段,服务器回应,会有一个Access-Control-Allow-Oringin头信息字段













