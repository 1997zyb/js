var stu = {
    0: 1,
    1: '李强',
    2: 12,
    3: 1.77,
    length:4
}
// let stus = Array.of(stu)
// console.log(stus);
// console.log(Object.keys(stu));
// console.log(Object.values(stu));


// from用于类数组，而且它的键名必须是数字，，还要有length属性
let stus = Array.from(stu)
console.log(stus);

let str = 'hello world'
console.log(Array.from(str));