// concat
var temp = ['a', 'b', 'c', 'd']
var temp2 = [1, 2, 3]
var res = temp.concat(temp2)
console.log(res);
console.log(temp);


// map
var temp = [1, 2, 3, 4]
var res = temp.map((item, index) => {
    return item * 2
})
console.log(res);
console.log(temp);

// forEach
var temp = [1, 2, 3, 4]
temp.forEach((item, index) => {
    var res = [];

    res.push(item * 2)
})
console.log(temp);
console.log(res);