// Array是构造数组的全局对象

var fruits = new Array('apple','banana')
console.log(fruits);

var car = new Array('baoma')
console.log(car);

var arr = new Array(2) 
console.log(arr);
// 如果是数字，且只有一个，则表示这个数组的长度
// 其他情况下都正常

var arr1 = new Array(1,2)
console.log(arr1);

// 遍历数组

// （1）
for(item in fruits){
    console.log("我爱吃"+fruits[item]);
}

// （2）
fruits.forEach((item,index) => {
    console.log("我有一个"+item);
});

// 找出某个元素在数组中的索引
console.log(fruits.indexOf('apple'));







