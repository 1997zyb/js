// 许多DOM属性和方法，返回的结果是NodeList实例或HTMLCollection实例；
// NodeList可以包含各种类型的节点，HTMLCollection只能包含HTML元素节点



// NodeList实例是一个类数组的对象，它的成员是节点对象。
// 通过以下方法可以得到NodeList实例
    // Node.childNodes
    // document.querySelectorAll()登节点搜索方法

// NodeList实例很像数组,可以使用length属性和forEach方法，
// 但是不能使用数组特有的方法，例如pop、push
// 转化为真正的数组：Array.prototype.call()

// NodeList具有keys()、values()、entries()方法



// HTMLCollection是一个节点对象的集合，只能包含元素节点，不能包含其它类型的节点
// 返回值是一个类数组对象，但是和NodeList不同，他不能使用forEach方法





















