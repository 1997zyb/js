// 所有DOM节点对象都继承了Node接口，拥有一些共同的属性和方法。这是DOM操作的基础。


// nodeType属性返回一个整数值，表示节点的类型


// Node.prototype.nodeName属性返回节点的名称，返回的是大写的标签名


// Node.prototype.nodeValue属性返回一个字符串，表示当前节点本身的文本值，可读写
// 只有文本节点(text)，注释节点(comment)和属性节点(attr)有文本值，其他返回是null


// Node.prototype.textContent属性返回当前节点和它所有后代节点的文本内容。
// textContent属性自动忽略当前节点内部的HTML标签



// Node.prototype.baseURI属性返回一个字符串，表示当前网页的绝对路径。



// Node.prototype.nextSibling属性返回紧跟当前节点后面的第一个同级节点







