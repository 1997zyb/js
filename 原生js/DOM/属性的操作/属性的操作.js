// HTML元素包含标签名和若干个键值对，这个键值对就称为‘属性’（attribute）

// 元素对象有一个attributes属性，返回一个类数组对象，
// 成员是该元素的所有属性节点对象




// 元素节点提供六个方法，用来操作属性

// Element.getAttribute()方法返回当前元素节点的指定属性，如果指定属性不存在，则返回null

// Element.getAttributeNames()方法返回一个数组，成员是当前元素的所有属性名
// 与Element.attributes返回的是一个类数组对象
 
// Element.setAttribute()方法用于当前元素节点新增属性，如果同名属性已存在，则相当于编辑已存在属性

// Element.hasAttribute()方法返回一个布尔值，表示当前元素节点是否包含指定属性

// Element.hasAttributes()方法返回一个布尔值，表示当前元素是否有属性

// Element.removeAttribute()方法移除指定属性。该方法没有返回值



