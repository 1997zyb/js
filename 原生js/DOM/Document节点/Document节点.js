// document节点对象代表整个文档



// document.defaultView属性返回document对象所属的window对象。



// document.doctype指文档类型
// document.documentElement属性返回当前文档的根元素节点
// document.body属性指向<body>节点
// document.head属性指向<head>节点



// document.activeElement属性返回获得当前焦点的DOM元素。通常，
// 这个属性返回的是<input>、<textarea>、<select>等表单元素





// 节点集合属性
// 以下属性返回一个HTMLCollection实例

// document.links属性返回当前文档所有设定了href属性的<a>及<area>节点
// document.forms属性返回所有<form>表单节点
// 例：var selectForm = document.forms[0]   获取第一个

// document.images属性返回页面所有<img>图片节点





// 文档静态信息属性
// 以下属性返回文档信息

// document.documentURI     document.URL
// 他们两个都返回一个字符串,表示当前文档的网址

// document.domain属性返回当前文档的域名，不包含协议和端口。
// document.lastModified属性返回一个字符串，表示当前文档最后修改时间






// 方法
// document.open()方法清除当前文档所有内容，供document.write方法写入内容
// document.close方法用来关闭document.open()打开的文档
// document.write和document.writeln用法一样,只是writeln会在后面加换行符




// document.querySelector()方法接受一个CSS选择器作为参数,返回匹配该选择器的元素节点
// document.querySelectorAll()返回的是一个NodeList对象,包含所有匹配的节点

// 例:var matches = document.querySelectorALL('div.note','div.alert')
// 上面代码返回class属性是note或alert的div元素




// document.getElementsByTagName()方法搜索HTML标签名,返回符合条件的元素
// 它的返回值是一个类数组对象 (HTMLCollection实例)

// document.getElementsByClassName()包含所有class名字符合指定条件的元素
// 它的返回值是一个类数组成员 (HTMLCollection实例)

// document.getElementsByName()方法用于选择拥有name的HTML元素
// 比如<form>,<radio>,<img>等

// document.getElementById()方法返回匹配指定id属性的元素节点


// document.createElement()方法用于生成元素节点,并返回该节点
// createElment方法的参数为元素的标签名

// document.createTextNode()方法用于生成文本节点

// document.createAttribute()方法生成一个新的属性节点，























