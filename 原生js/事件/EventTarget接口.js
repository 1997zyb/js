// addEventListener:绑定事件的监听函数
// EventTarget.addEventListener(type,listener[,useCapture])
// 该参数接受三个参数
    // -type：事件名称，大小写敏感
    // -listener：监听函数。事件发生时，会调用该监听函数
    // -useCapture：布尔值，表示监听函数是否在捕获阶段
function h(){
    console.log("hello");
}
var btn = document.getElementById('btn')
btn.addEventListener('click',h,false)

// 如果希望想监听函数传递参数，可以使用匿名函数包装一下监听函数
function print(x){
    console.log(x);
}
var el = document.getElementById('div1')
el.addEventListener('click',function(){print('hello');},false)
// 上面代码通过匿名函数，向监听函数print传递了一个参数
// 监听函数内部的this，指向当前事件所在的那个对象
var para = document.getElementById('para')
para.addEventListener('click',function(e){
    console.log(this);
},false)
// 上面的this就代表para


// 3.Eventarget.removeEventListener()
// 这个方法用来移除addEventListener方法添加的事件监听函数。该方法没有返回值
div.addEventListener('click',function(){

},false)
div.removeEventListener('click',function(){

},false)


// 4.EventTarget.dispatchEvent()
// 这个方法实在当前节点上触发指定事件，从而触发监听函数的执行
para.addEventListener('click',hello,false)
var e = new Event('click')
para.dispatchEvent(e)
// 如果diapatchEvent方法内的参数不是一个有效事件，或者为空，将报错





























