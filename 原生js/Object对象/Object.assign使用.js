const target = { a: 1, b: 2, arr: [1, 2, 3] }
const target2 = {}
// 将所有可枚举的属性从一个对象复制到另一个对象
// 并返回目标对象
// Object.assign(目标对象，源对象)
console.log(Object.assign(target2, target));
// console.log(target2);

// 浅拷贝
target2.arr[0] = 4
console.log(target);
console.log(target2);




const stu = {
    name: "李强",
    age: 10,
    sex: "男",
}
const stu2 = {
    name: "李白",
    height: 1.8
}
console.log(Object.assign(stu2, stu));
console.log(stu2);

let obj1 = { a: 0, b: { c: 1 } }
// let obj2 = Object.assign({},obj1)
// console.log(obj2);

let obj3 = JSON.parse(JSON.stringify(obj1))
obj3.b.c = 4
console.log(obj1);
console.log(obj3);





