// 直接在一个对象上定义新的属性或修改现有属性
var obj = {
    name: "赵云",
    age: 10,
}
Object.defineProperties(obj, {
    'sex': {
        value: "男",
        // 是否可枚举，为true时，才显示
        enumerable: true,
    },
    'height': {
        value: 1.77
    }
})
console.log(obj);