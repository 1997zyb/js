var arr = ['apple', 'banana', 'orange']
console.log(0 in arr); //true
console.log('apple' in arr); //false
// 可以知道，在数组中使用in运算符，必须使用索引，
for (item in arr) {
    console.log(item);
    // 得出item为0，1，2
    console.log(arr[item]);
}



var car = { mark: "Honda", model: "Accord", year: 11988 };
console.log(typeof car);
console.log('mark' in car);



var color1 = 'abcdefg';
console.log(color1.length);
console.log('length' in color1);
// in 右边必须是对象值