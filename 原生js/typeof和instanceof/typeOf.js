// 首先判断一下类型
console.log(typeof 3); // number
console.log(typeof 'abc'); // string
console.log(typeof true); // boolean
console.log(typeof undefined); // undefined
console.log(typeof function(){}); //function
console.log(typeof {}); // object

// 只有null返回的是object，其他都能返回对应的类型
console.log(typeof null); // object

// typeof操作符返回的是一个字符串，记住返回的是一个字符串。



var s = Number(1) // 没有new会解析为一个字符串
var t = new Number(1) //有了new，那他就是一个构造函数，t就是一个实例对象
console.log( typeof s);
console.log( typeof t);

var func = new Function();
console.log(typeof func); // function











console.log(typeof typeof(1)); // sting，因为typeof返回的是一个字符串。

console.log(typeof String(1));





console.log(typeof new Boolean(true) === 'object');



