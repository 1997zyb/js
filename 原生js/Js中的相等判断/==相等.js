// 双等号将会执行类型转换
console.log(2 == '2');
console.log(-0 == +0);



// 严格相等在比较时不会进行类型转换
console.log(-0 === +0)
var obj1 = {
    name:"李强",
    age:12,
}
var obj2 = {
    name:"李强",
    age:12,
}
console.log(obj1 === obj1);





console.log(-0 == +0);
console.log(-0 === +0);
console.log(Object.is(-0,+0));


// 非严格相等和严格相等子比较正0和负0时，都是true，只有Object.is返回false。
console.log(-0 == +0);
console.log(-0 === +0);
console.log(Object.is(-0,+0));




// 非严格相等和严格相等在比较NaN时，都是false，只有Object.is返回true。
console.log(NaN == NaN);
console.log(NaN === NaN);
console.log(Object.is(NaN,NaN));
// 可以看出Object.is()是比较两个值是否相同，



// 在非严格相等中
console.log(undefined == null); // true

// 在严格相等中
console.log((undefined === null));// false
// 可以理解为null的类型是object