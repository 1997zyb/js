// 1.对象就是 “键值对” 的集合，是无序的。即
var obj = {
    name:'张三',//键值对   键名   键值
    age:6,
    sex:'男',
}


// 对象的每一个键名又称 “属性”，它的 “键值”可以是任意数据类型
// 如果属性的值是函数，通常把这个属性称为 “方法”，可以像函数那样调用
var obj = { 
    p:function(x){
        return x*x;
    }
}
console.log(obj.p(2));


// 2.对象的引用
// 如果不同的变量名指向同一个对象，那么他们都是这个对象的引用，也就是说指向同一内存地址
// 修改其中一个变量，会影响到其他所有变量
var o1 = {}
var o2 = o1
o1.a = 1;
console.log(o2);
o2.b = 3;
console.log(o1);
// o1和o2指向同一个对象

// 但是，这种引用只局限于对象，如果两个变量指向同一原始类型的值。
// 那么，变量这时都是值的拷贝
var x = 1;
var y = x;
console.log(y);
var y = 6;
console.log(x,y);



// 3.属性的操作
// 3.1 读取对象的属性，有两种方法，一种使用点运算符；另一种使用方括号运算符
var obj = {
    p:'myName'
}
console.log(obj.p);
console.log(obj['p']);
// 注意：数值键名不能使用点运算符，只能使用方括号运算符
var obj = {
    123:"hello"
}
console.log(obj[123]);


// 3.2 属性的赋值     使用点运算符或者方括号运算符
var obj = {}
obj.foo = 'hello';
obj['bar'] = 'succeed'
console.log(obj);

// 3.3 属性的查看     查看一个对象本身的所有属性，可以使用Object.keys方法
var obj = {
    foo:'milk',
    bar:'water',
}
console.log(Object.keys(obj));

// 3.4 属性的删除：delte
delete obj.bar;
console.log(obj);
// 注意：删除一个不存在的属性也不会报错，只有一种情况会报错
// 即 该属性存在且不得删除
// delete只能删除对象本身的属性，无法删除继承的属性



// 4.属性是否存在：in运算符
// in运算符用于检查对象是否包含某个属性（注意：检查的是键名，不是键值）
// 可以使用hasOwnProperty方法判断，是否为对象本身的属性
var obj = {};
if('toString' in obj){
    console.log(obj.hasOwnProperty('toString'));
}


// 属性的遍历：for...in循环
var obj = {
    name:"张三",
    age:6,
    sex:"男"
}
for(i in obj){
    console.log(i + ':' + obj[i]);    
}





// 对象的引用
// 如果不同的变量名指向同一个对象，









